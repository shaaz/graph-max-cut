"""
Cutting plane factory methods.
Author: Shoeb Mohammed.
"""
import sys
import os
import time
import pdb

import graph
import utils
import numpy as np

# gprime is auxiliary graph structure explained in report
# Briefly, given a graph G(V,E), G'(V',E') is constructed by
# creating two copies u and u' of each vertex u in V. For
# each edge uv in E(G), create two edges uv' and u'v.
gprime = None

def make_violated_odd_cycle(x):
    """
    :param x: Vector x of form [x_e0, x_e1, ...,x_em]
              where 0 <= x_ei <= 1.

    :return: [ odd_cycle ]
             where x(odd_cycle) > |odd_cycle| - 1.
             Odd cycle is represented as list of edges.
             For example, odd_cycle = [e0, e1, e2].

    Note:
             (i) The edge indices ei for graph G are assumed
             to be [0...m-1].

             (ii) Current implementation returns a single
             odd-cycle.

    TO DO:
             return more than one odd cycle
             [ odd_cycle1, odd_cycle2, ...].
    """
    w = 1-np.array(x)
    w[w<0] = 0
    graph.update_auxiliary_graph_weights(gprime, w)
    dij = utils.Dijkstra(gprime)
    min_weight_cycle = np.inf
    min_weight_cycle_length = np.inf
    cycle_start_node = np.inf
    cycle = None
    g_nodes, g_edges = int(gprime.nodes/2), int(gprime.edges/2)

    # we only record cycle with min. weight.
    # This will give only one cut plane.
    ## TODO: if we record cycles with weight < 1, we should be
    ##       able to generate multiple cutting planes.
    for node in range(0, g_nodes):
        dij.run(start_node=node)
        cycle_weight = dij.y_p_len_S[node + g_nodes,0]
        cycle_length = dij.y_p_len_S[node + g_nodes,2]

        if cycle_weight < min_weight_cycle:
            min_weight_cycle = cycle_weight
            min_weight_cycle_length = cycle_length
            cycle_start_node = node
            cycle = np.copy(dij.y_p_len_S[:,1])

        elif cycle_weight == min_weight_cycle and cycle_length \
                < min_weight_cycle_length:
            min_weight_cycle_length = cycle_length
            cycle_start_node = node
            cycle = np.copy(dij.y_p_len_S[:,1])

    # list of edges comprising the violated odd-cycle
    odd_cycle = set()

    if cycle.size != 0 and min_weight_cycle < 1:
        current_node = int(cycle_start_node + g_nodes)
        while current_node != int(cycle_start_node):
            e = gprime.node_data[current_node][int(cycle[current_node])]
            odd_cycle.add(e % g_edges)
            current_node = int(cycle[current_node])

    return [list(odd_cycle)]