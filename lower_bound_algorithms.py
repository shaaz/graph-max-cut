"""
Lower bound algorithms.
Author: Alex Teich
"""
import numpy as np

def greedy_maintain_bipartite_for_lower_bound(n_m_data, edge_data):
    # ranks edges by weight, and then adds each if bipartiteness is maintained.
    a_nodes = []
    b_nodes = []
    m = n_m_data[1]
    x_output = np.zeros((m,), dtype=np.int)
    current_weight_total = 0
    edge_data = edge_data[-edge_data[:, 2].argsort()]

    for i in range(0, m):
        put_edge_in = 0
        assign_first_end = 0
        assign_second_end = 0
        if edge_data[i][0] in a_nodes:
            if not edge_data[i][1] in a_nodes:
                put_edge_in = 1
                if not edge_data[i][1] in b_nodes:
                    assign_second_end = 2
        elif edge_data[i][0] in b_nodes:
            if not edge_data[i][1] in b_nodes:
                put_edge_in = 1
                if not edge_data[i][1] in a_nodes:
                    assign_second_end = 1
        else:
            put_edge_in = 1
            if edge_data[i][1] in a_nodes:
                assign_first_end = 2
            elif edge_data[i][1] in b_nodes:
                assign_first_end = 1
            else:
                assign_first_end = 1
                assign_second_end = 2
        if put_edge_in == 1:
            x_output[i] = 1
            current_weight_total += edge_data[i][2]
        if assign_first_end == 1:
            a_nodes.append(edge_data[i][0])
        if assign_first_end == 2:
            b_nodes.append(edge_data[i][0])
        if assign_second_end == 1:
            a_nodes.append(edge_data[i][1])
        if assign_second_end == 2:
            b_nodes.append(edge_data[i][1])

    return x_output, current_weight_total


def greedy_nodewise_voting_lower_bound(n_m_data, edge_data):
    # adds nodes to sides of partition in numerical order. each decision is made based on which direction gives the higher weight total.
    # initialize
    a_nodes = [0]
    b_nodes = []
    n = n_m_data[0]
    m = n_m_data[1]
    x_output = np.zeros((m,), dtype=np.int)
    current_weight_total = 0
    end_one_is_zero = 1
    i = 0

    # start by putting node 0 in a, all nodes 1 away from 0 in b
    while end_one_is_zero:
        b_nodes.append(edge_data[i][1])
        current_weight_total += edge_data[i][2]
        x_output[i] = 1
        i += 1
        if i < len(edge_data):
            if edge_data[i][0] != 0:
                end_one_is_zero = 0
        else:
            end_one_is_zero = 0
    i = 1
    # iterate through the rest of the nodes (now i is a node number, not an array index)
    for i in range(1, n):
        wt_total_if_a = current_weight_total
        wt_total_if_b = current_weight_total
        i_declared = 0
        if i in a_nodes:
            i_undeclared = 1
        if i in b_nodes:
            i_undeclared = 2
        j = 0

        # For each node i: return to previously considered edges. if node i is placed on a side, find cost or benefit of i switching sides with respect to each edge. Otherwise, just use this loop to find the first row where node i is the first end of an edge.
        end_one_is_less_than_i = 1

        while end_one_is_less_than_i:
            if i_declared > 0:
                if edge_data[j][1] == i:
                    if i_declared == 1:
                        if edge_data[j][0] in a_nodes:
                            wt_total_if_b += edge_data[j][2]
                        else:
                            wt_total_if_b -= edge_data[j][2]
                    elif i_declared == 2:
                        if edge_data[j][0] in a_nodes:
                            wt_total_if_a -= edge_data[j][2]
                        else:
                            wt_total_if_a += edge_data[j][2]
            j += 1
            if j < len(edge_data):
                if edge_data[j][0] >= i:
                    end_one_is_less_than_i = 0
            else:
                end_one_is_less_than_i = 0
        # now determine cost/benefit for node i joining a and b, from within the lines beginning with node i. Index j is still active and now set to the appropriate value.
        end_one_is_i = 0
        if j < len(edge_data):
            if edge_data[j][0] == i:
                end_one_is_i = 1
        unassigned_nodes_adj_to_i = []
        while end_one_is_i:
            if edge_data[j][1] in a_nodes:
                wt_total_if_a += edge_data[j][2]
            if edge_data[j][1] in b_nodes:
                wt_total_if_b += edge_data[j][2]
            elif not edge_data[j][1] in a_nodes:
                wt_total_if_a += edge_data[j][2]
                wt_total_if_b += edge_data[j][2]
                unassigned_nodes_adj_to_i.append(edge_data[j][1])
            j += 1
            if j < len(edge_data):
                if edge_data[j][0] != i:
                    end_one_is_i = 0
            else:
                end_one_is_i = 0
        # we now have the weight of the cut we will get by assigning node i to either side a or side b.
        if wt_total_if_a >= wt_total_if_b:
            if not i in a_nodes:
                a_nodes.append(i)
            if i in b_nodes:
                b_nodes.remove(i)
        else:
            if not i in b_nodes:
                b_nodes.append(i)
            if i in a_nodes:
                a_nodes.remove(i)

                # we pass through the unassigned nodes adjacent to node i and put them on the other side of the partition.
        for k in unassigned_nodes_adj_to_i:
            if i in a_nodes:
                b_nodes.append(k)
            if i in b_nodes:
                a_nodes.append(k)

    number_of_turns = 3
    # more passes: iterate through all nodes to find an improvement.
    for turn in range(0, number_of_turns):
        for i in range(0, n):
            switch_benefit = 0
            for j in range(0, m):
                if edge_data[j][0] == i or edge_data[j][1] == i:
                    if edge_data[j][0] in a_nodes:
                        if edge_data[j][1] in a_nodes:
                            switch_benefit += edge_data[j][2]
                        elif edge_data[j][1] in b_nodes:
                            switch_benefit -= edge_data[j][2]
                    if edge_data[j][0] in b_nodes:
                        if edge_data[j][1] in b_nodes:
                            switch_benefit += edge_data[j][2]
                        elif edge_data[j][1] in a_nodes:
                            switch_benefit -= edge_data[j][2]
            if switch_benefit > 0:
                switch_to = 0
                if i in a_nodes:
                    switch_to = 1
                if switch_to == 0:
                    a_nodes.append(i)
                    b_nodes.remove(i)
                else:
                    b_nodes.append(i)
                    a_nodes.remove(i)

    # find vector and total weight based on partition.
    current_weight_total = 0
    for i in range(0, m):
        if edge_data[i][0] in a_nodes and edge_data[i][1] in b_nodes or edge_data[i][0] in b_nodes and edge_data[i][
            1] in a_nodes:
            x_output[i] = 1
            current_weight_total += edge_data[i][2]

    return x_output, current_weight_total
