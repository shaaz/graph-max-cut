""" Various utilities like Graph algorithms, file I/O
Author: Shoeb Mohammed, Varun S.
"""
import numpy as np


class GraphAlgorithm(object):
    """
    Common interface for graph algorithms
    """
    def run(self, **params):
        raise NotImplementedError('a sub-class must implement this method.')


class Dijkstra(GraphAlgorithm):
    """
    Implements shortest paths using Dijkstra's Algorithm.
    The current implementation uses a naive approach.
    """
    def __init__(self, graph):
        """
        :param graph: Graph data-structure
        """
        self.graph = graph

        # np array y_p_len_S is interpreted as:
        # 1st column is potential vector
        # 2nd column is parent node
        # 3rd column is path length
        # 4th column = inf means node scanned , otherwise not yet scanned
        self.y_p_len_S = np.full((graph.nodes, 4), np.inf)
        self.y_p_len_S[:, 3] = 0

    def run(self, start_node, **params):
        self.y_p_len_S[:] = np.inf
        self.y_p_len_S[:,3] = 0
        self.y_p_len_S[start_node, :] = 0

        for _ in range(0, self.graph.nodes):
            min_node = np.argmin(self.y_p_len_S[:,0] + self.y_p_len_S[:,3])
            self._scan(min_node)
            self.y_p_len_S[min_node,3] = np.inf

    def _scan(self, node):
        if self.y_p_len_S[node, 0] == np.inf:
            return

        for adj_node, e in self.graph.node_data[node].items():

            if self.y_p_len_S[node, 0] + self.graph.edge_data[e,2] \
                    < self.y_p_len_S[adj_node, 0]:
                self.y_p_len_S[adj_node,:3] = \
                    self.y_p_len_S[node,0] + self.graph.edge_data[e,2], \
                    node, self.y_p_len_S[node,2]+1

    def print_y_p_len_s(self, filename):
        with open(filename, 'w') as f:
            f.write('\n---Dijkstras [y p path_len inset]---\n')
            np.savetxt(f, self.y_p_len_S, fmt='%.2f')


class BFS(GraphAlgorithm):
    """
    Implements BFS on a graph.
    """
    def __init__(self, graph):
        self.graph = graph

    def run(self, start_node, **params):
        pass


def read_file(filename):
    """
    Copied from CAAM471 project code
    :param filename:
    :return:
    """
    # [Num_nodxses Num_edges]
    hard_to_read = 0
    with open(filename, 'r') as f:
        first_line = f.readline()
    if first_line[0] == ' ':
        hard_to_read = 1
    elif first_line[0] == '\n':
        hard_to_read = 2

    node_edge_data = np.array([int(x) for x in first_line.split()])

    if hard_to_read == 0:
        connect_data = np.loadtxt(filename, skiprows=1)
    elif hard_to_read == 1:
        with open(filename, 'r') as f:
            whole_file = f.readlines()
        n_edges = int(node_edge_data[1])
        connect_data = np.zeros((n_edges, 3))
        for i in range(1, 1 + n_edges):
            connect_data[i - 1, :] = np.array([int(x) for x in whole_file[i].split()])
    else:
        with open(filename, 'r') as f:
            whole_file = f.readlines()
        first_line = whole_file[1]
        node_edge_data = np.array([int(x) for x in first_line.split()])
        n_edges = int(node_edge_data[1])
        connect_data = np.zeros((n_edges, 3))
        for i in range(2, 2 + n_edges):
            connect_data[i - 2, :] = np.array([int(x) for x in whole_file[i].split()])

    return node_edge_data, connect_data


def write_to_solution_file(solution_file, opt_edges, MinObjVal):
    """
    Copied from CAAM471 project code.
    :param solution_file:
    :param opt_edges:
    :param MinObjVal:
    :return:
    """
    with open(solution_file, "w") as myfile:
        myfile.write('\n----------------\nend1 end2 weight \n----------------\n')
        for item in opt_edges:
            end1 = str(item[0]) + ' '
            end2 = str(item[1]) + ' '
            weight = str(item[2])
            offset1 = 6 - len(end1)
            offset2 = 6 - len(end2)
            myfile.write(end1 + ' ' * (offset1) + end2 + offset2 * ' ' + weight + '\n')

        myfile.write('\nThe cost of the best cut is: ' + str(MinObjVal))


def write_to_stdout(opt_edges, MinObjVal):
    """
    Copied from CAAM471 project code.
    :param opt_edges:
    :param MinObjVal:
    :return:
    """
    print('\n----------------\nend1 end2 weight \n----------------\n')
    for item in opt_edges:
        end1 = str(item[0]) + ' '
        end2 = str(item[1]) + ' '
        weight = str(item[2])
        offset1 = 6 - len(end1)
        offset2 = 6 - len(end2)
        print(end1 + ' ' * (offset1) + end2 + offset2 * ' ' + weight + '\n')

    print('\nThe cost of the best cut is: ' + str(MinObjVal))