""" Graph representation
Author: Shoeb Mohammed
"""


import numpy as np
from collections import defaultdict


class DiGraph(object):
    """
    Represents a directed graph.
    """
    def __init__(self, m=0, n=0, connect_data=None):
        """
        :param m: #edges
        :param n: #nodes
        :param connect_data: mx3 np array.
                 Each row is (tail, head, edge_weight).

                 The edges are indexed in the same order from 0 through m-1.
                 Nodes are also assumed to be indexed from 0 through n-1.
        """
        self.nodes = n
        self.edges = m
        self.edge_data = connect_data

        # indexed by nodes. Each entry is node adjacency list as a dict.
        # for example {node: {(other_node1: edge1), (other_node2: edge2) ...}}
        self.node_data = defaultdict(dict)

        for e in range(0, self.edges):
            self.node_data[int(self.edge_data[e,0])][int(self.edge_data[e,1])] \
                = e

    def print_to_file(self, filename):
        import json

        with open(filename, 'w') as f:
            f.write(str(self.nodes) + ' ' + str(self.edges))
        with open(filename, 'a') as f:
            f.write('\n---Edge data---\n')
            np.savetxt(f, self.edge_data, fmt='%.1f')
            f.write('\n---Node adjacency list---\n')
            f.write(json.dumps(self.node_data))

class Graph(DiGraph):
    """
    represents an undirected graph
    """
    def __init__(self, m=0, n=0, connect_data=None):
        """
        :param m: #edges
        :param n: #nodes
        :param connect_data: mx3 np array.
                 Each row is (end1, end2, edge_weight).
         """
        super(Graph, self).__init__(m, n, connect_data)

        # for undirected graph both nodes appear in mutual adjacency lists.
        self.node_data = defaultdict(dict)
        for e in range(0, self.edges):
            self.node_data[int(self.edge_data[e,0])][int(self.edge_data[e,1])] \
                = e
            self.node_data[int(self.edge_data[e,1])][int(self.edge_data[e,0])] \
                = e


def make_auxiliary_graph(graph):
    """
    :param graph: undirected graph G(V,E)
    :return: a graph G'(V',E') as described in report.

           Briefly, given a graph G(V,E), G'(V',E') is constructed by
           creating two copies u and u' of each vertex u in V. For
           each edge uv in E(G), create two edges uv' and u'v in E(G').

           Nodes in G' are indexed as
           index(u) in G' = index(u) in G.
           index(u') in G' = index(u) + n.

           Edges in G' are indexed from 0 through 2m-1.
           index(uv') = index(uv).
           index(u'v) = index(uv) + m.
    """
    aux_graph_connect_data = np.vstack((graph.edge_data, graph.edge_data))
    aux_graph_connect_data[:graph.edges,1] += graph.nodes  # create edges uv'
    aux_graph_connect_data[graph.edges:,0] += graph.nodes  # create edges u'v

    return Graph(2*graph.edges, 2*graph.nodes, aux_graph_connect_data)


def update_auxiliary_graph_weights(aux_graph, graph_weights):
    """
    :param aux_graph: graph G' as described in report
    :param graph_weights: weights of edges corresponding to graph G
    :return: None
    """
    aux_graph.edge_data[:,2] = np.hstack((graph_weights, graph_weights))