"""
CAAM571 project: Branch and Cut implementation.
Author: Shoeb Mohammed.
"""
import sys
from collections import deque
import numpy as np
import random

import gurobipy as gurobi


class BranchAndCut(object):
    def __init__(self,
                 init_lp,
                 cutting_plane_factory,
                 lower_bound=-np.inf,
                 max_cuts_per_node=30):
        """
        :param init_lp: initial formulation of the LP.
        :param cutting_plane_factory: factory that produces cuts.
                The factory takes two input parameters (x, #number_of_cuts_requested).
                where x is the current solution from the LP.
        :param lower_bound: initial global lower bound to the optimum solution.
        :param max_cuts_per_node: This limits the number of cuts applied
                at a node of the branch and bound tree. If after applying
                these cuts we still get a fractional solution, we branch.
                If we get an integral solution, this node is pushed back
                in the queue of sub-problems and again solved sometime
                later.
        """
        self.init_lp = init_lp
        self.cutting_plane_factory = cutting_plane_factory
        self.lower_bound = lower_bound
        self.x_lower_bound = None
        self.status = 'not_started'
        self.max_cuts_per_node = max_cuts_per_node

    def run(self):
        """
        Branch and cut routine.

        TODO: add a parameter for early termination like max_tree_depth,
              tolerance, iterations etc
        """
        self.status = 'failed'
        sub_problem_queue = deque([[[]]])

        # Only if we want extra functionality to stop early
        # before searching full tree.
        # tree_depth = 0

        # Only if we want extra functionality to return best_lp
        # that generated best X and Objective.
        # lp_best = []

        while sub_problem_queue:
            sub_problem = sub_problem_queue.popleft()
            return_status, x, obj = self._sub_problem_processor(sub_problem)

            if return_status == 'optimum_and_integral' and obj >= self.lower_bound:
                self.lower_bound = obj
                self.x_lower_bound = np.round(x)
                self.status = 'best_lower_bound'
            elif return_status == 'prune' or return_status == 'failed':
                continue
            elif return_status == 'branch_and_integral':
                sub_problem_queue.append(sub_problem)
            else:
                branch_index = np.argmin(np.abs(np.array([xi for xi in x])-0.5))
                left_branch = sub_problem + [[(1, branch_index), 0]]
                right_branch = sub_problem + [[(-1, branch_index), -1]]
                sub_problem_queue.extend([left_branch, right_branch])

        if not sub_problem_queue and self.status == 'best_lower_bound':
            self.status = 'optimum'

    def _sub_problem_processor(self, sub_lp):
        """
        This method solves a sub-problem, that is, a problem in
        a branch and bound tree node, by repeatedly applying cuts.

        :param sub_lp: constraints to apply on top of init_lp
                       to get the sub-problem.
        :return:
        """
        x = self.init_lp.getVars()
        obj = -np.inf
        constrs_gurobi_format = []
        # print('Started subproblem')
        # print(sub_lp)
        # To create gurobi model for the sub_lp, apply all
        # constraints in sub_lp on top of init_lp. Note
        # that constraints in sub_lp are in our format.
        for constr in sub_lp:
            if constr:
                constr_gurobi_format = self.init_lp.addConstr(
                    sum(c*x[i] for c, i in constr[:-1]) <= constr[-1]
                )
                constrs_gurobi_format.append(constr_gurobi_format)

        self.init_lp.update()
        self.init_lp.optimize()

        all_cuts = 0
        # keep solving the sub-problem by adding cuts as long as possible.
        while self.init_lp.status == 2:
            obj_val = self.init_lp.ObjVal
            # print('subLP: cutting')

            if obj_val < self.lower_bound: # prune this branch and move on
                subproblem_status = 'prune'
                break
            else:
                x = self.init_lp.getVars()
                cuts = []
                if all_cuts < self.max_cuts_per_node:
                    cuts = self.cutting_plane_factory(
                        [xi.X for xi in x],
                        self.max_cuts_per_node-all_cuts
                    )
                    # print('subLP: cut plane factory returned cuts')
                    # print(cuts)

                if cuts:
                    for cut in cuts:
                        self.init_lp.addConstr(
                            sum(c*x[i] for c,i in cut[:-1]) <= cut[-1]
                        )
                    all_cuts += len(cuts)
                    self.init_lp.update()
                    self.init_lp.optimize()
                    continue
                elif np.all(np.isclose([xi.X for xi in x], 0) +
                                    np.isclose([xi.X for xi in x], 1)):
                    # x is integral.
                    cut_exists = self.cutting_plane_factory(
                        [xi.X for xi in x],
                        1
                    )
                    if not cut_exists:
                       subproblem_status = 'optimum_and_integral'
                       print('++++subproblem found A OPT+INTEGRAL++++')
                       print("\t objval: \t{}".format(obj_val))
                       print("\t best LB: \t{}".format(self.lower_bound))
                    else:
                        subproblem_status = 'branch_and_integral'
                        print("\t **subproblem branching on integral** \t" +
                              "(because number cutting planes exceeded threshold)")
                        print("\t\t objval: \t{}".format(obj_val))
                        print("\t\t best LB: \t{}".format(self.lower_bound))
                    # TODO: need to do save cuts and constraints in best_lp
                    #       This is only needed if we wanted to include
                    #       this functionality.
                    break
                else:
                    subproblem_status = 'branch_and_nonintegral'
                    break

        if self.init_lp.status != 2:
            subproblem_status = 'failed'
        else:
            x = self.init_lp.X
            obj = self.init_lp.ObjVal

        self.init_lp.remove(constrs_gurobi_format)
        self.init_lp.update()

        return subproblem_status, x, obj


def initialize_maxcut_lp(graph):
    """
    :param graph: graph data-structure
    :return: initialized gurobi model.

             Sum(w_e*x_e)
             subject to:
             0 <= x_e <= 1
             x_e1 + ... + x_en <= n^2/4
             x(C) <= |C| -1 for all odd cycles C.

             note: we do not add any odd cycles for initial LP formulation.
    """
    maxcut_lp = gurobi.Model()
    maxcut_lp.setParam('OutputFlag', False)
    x = maxcut_lp.addVars(range(graph.edges),
                          lb=0.0,
                          ub=1.0,
                          obj=list(graph.edge_data[:,2]),
                          vtype=gurobi.GRB.CONTINUOUS,
                          name='x')

    # maximum number of edges in a bipartite graph with n nodes is
    # n^2/4.
    maxcut_lp.addConstr(
        sum(x[i] for i in range(0, graph.edges)) <= graph.nodes**2/4
    )
    maxcut_lp.ModelSense = gurobi.GRB.MAXIMIZE
    maxcut_lp.update()
    return maxcut_lp