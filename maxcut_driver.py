"""
CAAM571 project: Max-cut using Branch and Cut procedure.
Author: Shoeb Mohammed.
"""
import sys
import time
import numpy as np

import graph
import utils
import cutting_plane_factory
import lower_bound_algorithms
import branch_and_cut as bnc

start = time.time()

# parse input data.
filename = sys.argv[1]
node_edge_data, connect_data = utils.read_file(filename)

# create graph data-structures.
g = graph.Graph(int(node_edge_data[1]),
                int(node_edge_data[0]),
                connect_data)
gprime = graph.make_auxiliary_graph(g)

# initialize cutting plane factory to produce
# cuts for graph gprime.
cutting_plane_factory.gprime = gprime

# formulate initial LP for maxcut.
maxcut_lp = bnc.initialize_maxcut_lp(graph=g)

# get lower bounds from lower bound algorithms (heuristics).
# Another possible heuristic to use here is
# 'greedy_maintain_bipartite_for_lower_bound'.
# See the module lower_bound_algorithms for all available
# heuristics.
x_lower_bound, obj_lower_bound = \
    lower_bound_algorithms.greedy_nodewise_voting_lower_bound(
        node_edge_data,
        connect_data
    )


# set up branch and cut.
def odd_cycle_cut_factory(x, max_cuts):
    """
    A wrapper around cutting plane factory to
    generate cuts in the form required by BranchAndCut.
    """
    cuts = cutting_plane_factory.make_violated_odd_cycle(x)
    if len(cuts) > max_cuts:
        cuts = cuts[:max_cuts]
    return [[(1, i) for i in cut] + [len(cut)-1] for cut in cuts if cut]

branch_and_cut = bnc.BranchAndCut(maxcut_lp,
                                  odd_cycle_cut_factory,
                                  obj_lower_bound-1,
                                  max_cuts_per_node=10)

# execute branch and cut procedure.
branch_and_cut.run()

# print outputs
print('\n---Output----\n')
print('greedy lower bound: {}'.format(obj_lower_bound))

print('\n---solution from branch-and-cut---\n')
print('status: {}'.format(branch_and_cut.status))
maxcut_edges =np.array([int(e) for e in branch_and_cut.x_lower_bound])

if branch_and_cut.status == 'optimum':
    print('optimum value (best cut): {}'.format(branch_and_cut.lower_bound))
    utils.write_to_solution_file(
        '{}_solution.txt'.format(filename.replace('.txt', '')),
        g.edge_data[maxcut_edges==1].astype(int),
        branch_and_cut.lower_bound
    )

    utils.write_to_stdout(
        g.edge_data[maxcut_edges==1].astype(int),
        branch_and_cut.lower_bound
    )

end = time.time()
print('\nTime = {} seconds'.format(end - start))