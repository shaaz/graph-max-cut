"""
Test script for odd cycle cuts.
Author: Shoeb Mohammed.
"""
import sys
import os
import numpy as np

import graph
import utils
import cutting_plane_factory

# Reading in Data
filename = sys.argv[1]
node_edge_data, connect_data = utils.read_file(filename)

g = graph.Graph(node_edge_data[1], node_edge_data[0], connect_data)
g.print_to_file('debug_'+os.path.basename(sys.argv[1]))

gprime = graph.make_auxiliary_graph(g)
cutting_plane_factory.gprime = gprime
gprime.print_to_file('auxdebug_'+os.path.basename(sys.argv[1]))

# testing on figure 5.26 in the text
# edges encoded in this test as
# {ab, ac, ae, bc, be, cd, de} <-> {0,1,2,3,4,5,6}
#
x1 = [1, 1, 0, 1, 0, 0, 0]  # violates cycle abca
x2 = [1, 0, 1, 0, 1, 0, 0]  # violates cycle abea
x3 = [1, 0, 1, 1, 0, 1, 1]  # violates cycle abcdea
x4 = [1, 1, 0, 0, 0, 0, 0]  # violates no odd cycle
x5 = [0, 0, 0, 1, 1, 1, 1]  # corresponds to even cycle bcdeb
x6 = [1, 1, 1, 1, 1, 1, 1]  # violates more than one odd cycle


print('\n\n---characteristic vector: abca---')
print(x1)
odd_cycle = cutting_plane_factory.make_violated_odd_cycle(x1)
print('---Violating Odd cycle---')
print(odd_cycle)


print('\n\n---characteristic vector: abea---')
print(x2)
odd_cycle = cutting_plane_factory.make_violated_odd_cycle(x2)
print('---Violating Odd cycle---')
print(odd_cycle)


print('\n\n---characteristic vector: abcdea---')
print(x3)
odd_cycle = cutting_plane_factory.make_violated_odd_cycle(x3)
print('---Violating Odd cycle---')
print(odd_cycle)


print('\n\n---characteristic vector: ab, ac---')
print(x4)
odd_cycle = cutting_plane_factory.make_violated_odd_cycle(x4)
print('---Violating Odd cycle---')
print(odd_cycle)


print('\n\n---characteristic vector: even cycle bcdeb---')
print(x5)
odd_cycle = cutting_plane_factory.make_violated_odd_cycle(x5)
print('---Violating Odd cycle---')
print(odd_cycle)

print('\n\n---characteristic vector: all edges (violates more than one odd cycle)---')
print(x6)
odd_cycle = cutting_plane_factory.make_violated_odd_cycle(x6)
print('---Violating Odd cycle---')
print(odd_cycle)