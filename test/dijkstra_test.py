"""
Test script for Dijkstra's algorithm.
Author: Shoeb Mohammed.
"""
import sys
import os

import graph
import utils


# Reading in Data
filename = sys.argv[1]
node_edge_data, connect_data = utils.read_file(filename)

g = graph.DiGraph(node_edge_data[1], node_edge_data[0], connect_data)
g.print_to_file('debug_'+os.path.basename(sys.argv[1]))

# auxg = graph.make_auxiliary_graph(g)
# auxg.print_to_file('auxdebug_'+os.path.basename(sys.argv[1]))

dj = utils.Dijkstra(g)
dj.run(start_node=0)  # assume start node = 0
dj.print_y_p_len_s('dksdebug_'+os.path.basename(sys.argv[1]))